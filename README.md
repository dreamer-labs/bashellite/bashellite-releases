# bashellite-releases

## Introduction

Official release definitions for Bashellite are documented in this repository inside of `releases.yml`.

This file contains a YAML dictionary called `releases` with every release of Bashellite.

The official installer for each release uses parts of this file to determine which versions of which components comprise each official release.

## Releases

### Release Codename

The `releases` dictionary contains one dictionary per release codename, using the release's codename as the dictionary name.

```
---
releases:
  release_name_here:
...
```

Releases are named alphabetically after some of our favorite things. :-)

```
---
releases:
  accio:
...
```

Each of these release codename dictionaries contain two sub-dictionaries. `info` and `components`.

### Info

The `info` dictionary in each release codename dictionary contains metadata about the release.

```
---
releases:
  accio:
    info:
...
```

#### Number

The `number` variable contains an integer with the numerical release number associated with the release codename.

```
---
releases:
  accio:
    info:
      number: 1
...
```

#### Released

The `released` variable contains a boolean. If true, the release is stable. If false, the release is a work-in-progress.

```
---
releases:
  accio:
    info:
      number: 1
      released: false
...
```

#### Components

The `components` subdictionary contains information about each component of the release. This includes things like `installer` and `docs`, which each having their own configuration files within each release's subdirectory within this repository.

```
---
releases:
  accio:
    info:
      number: 1
      released: false
    components:
      installer:
        uri: "git@gitlab.com:dreamer-labs/bashellite/bashellite-installer.git"
        version: "v1.0.0"
        config: "accio/installer.yml"
      docs:
        uri: "git@gitlab.com:dreamer-labs/bashellite/bashellite.dreamer-labs.net.git"
        version: "v1.0.0"
        config: "accio/docs.yml"
...
```

#### Installer Config File

The value of `releases.${release_name_here}.components.installer.config` is a file containing configuration data feed into the installer located at `uri` with version `version`. The `installer` dictionary in this file lists each component that the installer downloads and installs along with instructions for the installer on how to retrieve it. The installer is `ansible` based, and therefore uses `args` passed to different ansible modules defined within the installer repository. The installer may pull from source code, docker repositories, or other sources.

```
---
installer:
  bashellite:
    args:
      uri: "git@gitlab.com:dreamer-labs/bashellite/bashellite.git"
      version: "master"
  bashellite-providers:
    args:
      uri: "git@gitlab.com:dreamer-labs/bashellite/bashellite-providers.git"
      version: "master"
  bashellite-configs:
    args:
      uri: "git@gitlab.com:dreamer-labs/bashellite/bashellite-configs.git"
      version: "master"
...
...
```

#### Docs Config File

The value of `releases.${release_name_here}.components.docs.config` is a file containing configuration data feed into the docs builder located at `uri` with version `version`. The `docs` dictionary in this file lists each component that the docs builder downloads and uses. The docs builder is `python` based, and uses the `uri` passed in this file to pull a series of git repositories and/or files that it then feeds into `sphinx` for documentation rendering.

```
---
docs:
  - name: "bashellite"
    uri: "git@gitlab.com:dreamer-labs/bashellite/bashellite.git"
    version: "master"
  - name: "bashellite-providers"
    uri: "git@gitlab.com:dreamer-labs/bashellite/bashellite-providers.git"
    version: "master"
  - name: "bashellite-configs"
    uri: "git@gitlab.com:dreamer-labs/bashellite/bashellite-configs.git"
    version: "master"
...
```
